# TP sémantique

## Nécessaire

Pour l'utilisation de ce programme la librairie `rdflib` est nécessaire  
on peut l'installer via la commande `pip install rdflib`
La librairie `nltk` est aussi nécessaire si on veut utiliser la comparaison par synonymie.


## Utilisation 

En executant main.py on obtient l'interface de l'application 
Il faut choisir le fichier source et le fichier target, et le fichier référence ( en format RDF ) avec les 3 boutons du haut. ( Il faut bien faire attention à respecter le placement du fichier source et du fichier target ou cela causera une erreur lors de la vérification)
Ensuite, une méthode de mesure doit être choisie avec le menu déroulant de gauche.
Enfin, il faut entrer la ou les propriétés et le seuil dans les champs de saisie. Les propriétés, si elles sont plusieurs, doivent être séparées par des ";".
En cliquant sur le bouton "comparer", le programme compare les 2 graphes, et renvoie les valeurs de rappel, de précision et la f-measure.

Lors de la comparaison de gros fichiers l'application ne réponds pas pendant les calculs, ceci peut prendre quelques instants. 
