#début

def entetecomparaison(file):
    file.write("""<?xml version="1.0" encoding="UTF-8"?> 
<rdf:RDF xmlns="http://knowledgeweb.semanticweb.org/heterogeneity/alignment" 
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
xmlns:xsd="http://www.w3.org/2001/XMLSchema#"> 
<Alignment> 
<xml>yes</xml> 
<level>0</level> 
<type>??</type>\n""")
    

#Fin
def fincomparaison(file):
    file.write("""</Alignment>
</rdf:RDF>""")

#Milieu

def output(e1,e2,file_list,file_rdf):
    file_list.write(f"{e1} owl:sameAs {e2}\n")
    file_rdf.write(f"""<map>
<Cell>
<entity1 rdf:resource="{e1}"/>
<entity2 rdf:resource="{e2}"/>
<measure rdf:datatype="xsd:float">1.0</measure>
<relation>=</relation>
</Cell>
</map>\n""")


