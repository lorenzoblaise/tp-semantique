import rdflib
from rdflib.compare import to_isomorphic, graph_diff
#graph = rdflib.Graph("default")
#graph.open("store", create=True)
#graph.parse("resDHT.rdf.txt",format="xml")

# print out all the triples in the graph
#for subject, predicate, object in graph:
 #   print (subject, predicate, object)
def verification(fichier1, fichier2,seuil,methode,prop):
    output = f"output/verif/{methode}.txt"
    foutput=open(output,"a")

    vrai: float = 0
    total: float = 0
    faux1: float = 0
    faux2: float = 0

    ref = rdflib.Graph()
    ref.open("store", create=True)
    ref.parse(fichier1)
    res = rdflib.Graph()
    res.open("store", create=True)
    res.parse(fichier2) 

    iso1 = to_isomorphic(ref)
    iso2 = to_isomorphic(res)
    in_both, only_in_first, only_in_second = graph_diff(iso1, iso2)
    values1 = set()
    values2 = set()

    for s, p, o in in_both:
        total+=1
        vrai+=1

    for s, p, o in only_in_second:
        total+=1
        faux2 +=1

    for s, p, o in only_in_first:
        total+=1
        faux1 +=1

    precision:float = vrai/(vrai+faux1)
    recall:float = vrai/(vrai+faux2)
    fmesure:float = (2*precision*recall)/(precision+recall)


    print("vrai/total :" , vrai/total)
    print ("erreur de niveau 2 : " , faux2/total)
    print ("erreur de niveau 1 : ", faux1/total)
    print("precision :", precision)
    print("recall :", recall)
    print("fmesure :" , fmesure)
    foutput.write(f"{seuil} \t {fmesure} \t {recall} \t {precision} \t {prop}\n")
    return f" fmesure = {fmesure} \n recall= {recall}\n precision= {precision}"
