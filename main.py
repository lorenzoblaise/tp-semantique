from rdflib import *
from tkinter import *
from tkinter.filedialog import askopenfilename
from rdflib.namespace import NamespaceManager
from rdflib.namespace import RDF
import comparaisonSimilarity,ecriture,verification


global nom1,filename1, nom2, filename2, nom3, filename3
choices = ['Levenshtein','Jaro','NGram','Synonymy','Jaccard','MongeElkan','Jaro-Lev','Lev-NGram','Lev-Jaro-Ngram','Jaccard-MongeElkan','Toutes mesures']

#gestion refixs


def requete(entree):
    entete=(entree.split(':')[0])
    prefixes = {
    "ecrm":"http://erlangen-crm.org/current/",
    "efrbroo":"http://erlangen-crm.org/efrbroo/",
    "mus":"http://data.doremus.org/ontology#",
    "rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#",
} 
    if (entete!="http"):
        return f"""{prefixes[str(entete)]}{entree.split(':')[1]}"""
    return entree

#bouton 
def bouton_comparer ():
    
    g1 = Graph()
    g1.parse(filename1)
    g2 = Graph()
    g2.parse(filename2)
    choix = choix_methode.get()
    if choix == "Absolue":
        retourlabel['text']=comparaisonSimilarity.Absolue(filename1,filename2,0.8)
    elif choix=="verification":
        retourlabel['text']=verification.verification(filename1,filename2)
    else:
        output_list = f"output/list/{choix_methode.get()}{seuil_choix.get()}.txt"
        output_rdf = f"output/rdf/{choix_methode.get()}{seuil_choix.get()}.rdf"
        foutput_list =open(output_list,"w")
        foutput_rdf =open(output_rdf,"w")
        listeProp = []
        listeProp=entree_choix.get().split(';')
        ecriture.entetecomparaison(foutput_rdf)
        print (f"le seuil est {float(seuil_choix.get())}")
        ListeOutput=[]
        for p in listeProp:
            ListTemp=[]
            prop=p
            p=requete(p)
            print(f"{p}")
            ListeS1=[]
            ListeS2=[]
            for s1,p1,o1 in g1:
                if str(p1)== p:
                    ListeS1.append([s1,o1])
            for s2,p2,o2 in g2:
                if str(p2)==p:
                    ListeS2.append([s2,o2])

            for e1 in ListeS1:
                for e2 in ListeS2:
                    if choix == "Levenshtein":
                        if comparaisonSimilarity.LevNormalized(e1[1],e2[1]) > float(seuil_choix.get()):
                            if([e1[0],e2[0]] not in ListTemp):
                                ListTemp.append([e1[0],e2[0]])
                    
                    elif choix == "Jaro":
                        if comparaisonSimilarity.Jaro(e1[1],e2[1]) > float(seuil_choix.get()):
                           if([e1[0],e2[0]] not in ListTemp):
                                ListTemp.append([e1[0],e2[0]])

                    elif choix == "NGram":
                        if comparaisonSimilarity.NG(e1[1],e2[1],3) > float(seuil_choix.get()):
                           if([e1[0],e2[0]] not in ListTemp):
                                ListTemp.append([e1[0],e2[0]])

                    elif choix == "Synonymy":
                        if comparaisonSimilarity.SynonymySimilarity(e1[1],e2[1]) > float(seuil_choix.get()):
                            if([e1[0],e2[0]] not in ListTemp):
                                ListTemp.append([e1[0],e2[0]])

                    elif choix == "Jaccard":
                        if comparaisonSimilarity.ExtendedJaccard(e1[1],e2[1],0.75) > float(seuil_choix.get()):
                            if([e1[0],e2[0]] not in ListTemp):
                                ListTemp.append([e1[0],e2[0]])

                    elif choix == "MongeElkan":
                        if comparaisonSimilarity.MongeElkanSymmetric(e1[1],e2[1]) > float(seuil_choix.get()):
                            if([e1[0],e2[0]] not in ListTemp):
                                ListTemp.append([e1[0],e2[0]])
                    
                    elif choix == "Jaro-Lev":
                        Jaro=comparaisonSimilarity.Jaro(e1[1],e2[1])
                        Lev=comparaisonSimilarity.LevNormalized(e1[1],e2[1])
                        if (Jaro+Lev)/2 > float(seuil_choix.get()):
                            if([e1[0],e2[0]] not in ListTemp):
                                ListTemp.append([e1[0],e2[0]])
                    
                    elif choix == "Lev-NGram":
                        Lev=comparaisonSimilarity.LevNormalized(e1[1],e2[1])
                        NGram=comparaisonSimilarity.NG(e1[1],e2[1],3)
                        if (Lev+NGram)/2 > float(seuil_choix.get()):
                            if([e1[0],e2[0]] not in ListTemp):
                                ListTemp.append([e1[0],e2[0]])
                    
                    elif choix == "Jaccard-MongeElkan":
                        Jac=comparaisonSimilarity.ExtendedJaccard(e1[1],e2[1],0.75)
                        Monge=comparaisonSimilarity.MongeElkanSymmetric(e1[1],e2[1])
                        if (Jac+Monge)/2 > float(seuil_choix.get()):
                            if([e1[0],e2[0]] not in ListTemp):
                                ListTemp.append([e1[0],e2[0]])
                    
                    elif choix == "Lev-Jaro-Ngram":
                        Lev=comparaisonSimilarity.LevNormalized(e1[1],e2[1])
                        Jaro=comparaisonSimilarity.Jaro(e1[1],e2[1])
                        NGram=comparaisonSimilarity.NG(e1[1],e2[1],3)
                        if (Lev+Jaro+NGram)/3 > float(seuil_choix.get()):
                            if([e1[0],e2[0]] not in ListTemp):
                                ListTemp.append([e1[0],e2[0]])
                    
                    elif choix == "Toutes mesures":
                        Lev=comparaisonSimilarity.LevNormalized(e1[1],e2[1])
                        Jaro=comparaisonSimilarity.Jaro(e1[1],e2[1])
                        NGram=comparaisonSimilarity.NG(e1[1],e2[1],3)
                        Jac=comparaisonSimilarity.ExtendedJaccard(e1[1],e2[1],0.75)
                        Monge=comparaisonSimilarity.MongeElkanSymmetric(e1[1],e2[1])
                        if (Lev+Jaro+NGram+Jac+Monge)/5 > float(seuil_choix.get()):
                            if([e1[0],e2[0]] not in ListTemp):
                                ListTemp.append([e1[0],e2[0]])


            if prop==listeProp[0]:
                ListeOutput=ListTemp
            else:
                ListUnion=[]
                '''
                for e in ListTemp:
                    if e in ListeOutput:
                        print(ListUnion)
                        ListUnion.append(e)'''
                ListUnion = [value for value in ListeOutput if value in ListTemp]
                ListeOutput=ListUnion
        
        for e in ListeOutput:
            ecriture.output(e[0],e[1],foutput_list,foutput_rdf)
        
        ecriture.fincomparaison(foutput_rdf)                                     

        
        foutput_list.close()
        foutput_rdf.close()
        retourlabel['text']=verification.verification(filename3,output_rdf,float(seuil_choix.get()),choix,listeProp) 
    """    retourlabel['text']=f"utilisation de {choix_methode.get()} pour comparer {nom1} et {nom2}"
    print(f"utilisation de {choix_methode.get()} pour comparer {nom1} et {nom2}") """
    


def UploadAction1(event=None):
    global nom1,filename1

    filename1 = askopenfilename()
    # Cut path to the file off
    nom1 = filename1.split('/')[len(filename1.split('/'))-1]
    button1['text'] = nom1



def UploadAction2(event=None):
    global nom2,filename2
    filename2 = askopenfilename()
    # Cut path to the file off
    nom2 = filename2.split('/')[len(filename2.split('/'))-1]
    button2['text'] = nom2

def UploadAction3(event=None):
    global nom3, filename3
    filename3 = askopenfilename()
    # Cut path to the file off
    nom3 = filename3.split('/')[len(filename3.split('/'))-1]
    button3['text'] = nom3
 

#création de la fenêtre
fenetre = Tk()
fenetre.title("comparateur RDF")
fenetre.minsize(480,360)



frame = Frame(fenetre)
frame1=Frame(frame)
frame2=Frame(frame)
frame3=Frame(frame)
#frame 1
button1 = Button(frame1,text='Choisir source', command=UploadAction1, bg='brown', fg='white')
button1.pack(side=LEFT,padx=2, pady=5)
button2 = Button(frame1,text='Choisir target', command=UploadAction2, bg='brown', fg='white')
button2.pack(side=LEFT,padx=2, pady=5)
button3 = Button(frame1,text='Choisir reference', command=UploadAction3, bg='brown', fg='white')
button3.pack(side=RIGHT,padx=2, pady=5)



# frame 2
choix_methode = StringVar()
choix_methode.set('choisir une méthode')
w = OptionMenu(frame2, choix_methode, *choices)
w.pack(side=LEFT)

propriete_choix=StringVar()
propriete_choix.set("propriétés séparées par un ;")
entree_choix= Entry(frame2,textvariable=propriete_choix, width=30)
entree_choix.pack()

seuil_choix=StringVar()
seuil_choix.set("choisir un seuil")
entree_s_choix= Entry(frame2,textvariable=seuil_choix, width=30)
entree_s_choix.pack(side=RIGHT)


buttonE = Button(frame3,text='Comparer', bg='brown', fg='white', command= bouton_comparer)
buttonE.pack(padx=2, pady=5)

retourlabel = Label(frame,text="")
retourlabel.pack()

#ajouter
frame.pack(expand=YES)
frame1.pack(fill=BOTH)
frame2.pack()
frame3.pack()

fenetre.mainloop()


