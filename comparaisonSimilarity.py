from rdflib import *
from rdflib.compare import to_isomorphic, graph_diff
import nltk
### nltk.download('punkt') si on veut utiliser la tokénisation de nltk
### nltk.download() pour télécharger wordnet ! ###
from nltk.corpus import wordnet as wn
import sys


def Absolue (file1,file2,seuil):

    # Chargez les fichiers RDF en entrée dans deux graphes distincts
    g1 = Graph()
    g1.parse(file1)

    g2 = Graph()
    g2.parse(file2)

    # Calculez la similarité des deux graphes à l'aide de la méthode graph_diff()
    diff = graph_diff(g1,g2)

    # Déterminez la similarité des deux graphes en comparant le nombre de triplets communs à leur taille totale
    nb_triples_communs = len(diff[1])
    taille_totale = len(g1) + len(g2) - nb_triples_communs
    similarite = nb_triples_communs / taille_totale

    # Afficher le résultat
    if similarite >= float(seuil):
        return(f"Les deux graphes ont une similarité de {format(similarite)}")
    else:
        return(f"Les deux graphes ont une similarité de {format(similarite)}")


def Levenshtein(s1,s2):
    if len(s1) < len(s2):
        return Levenshtein(s2, s1)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    
    return previous_row[-1]


def LevNormalized(s1,s2):
    d = Levenshtein(s1,s2)
    return 1-(d/max(len(s1),len(s2)))




def Jaro(s1,s2):
    m=0
    t=0
    memeposition=0
    doublon_s1=[]
    doublon_s2=[]

    if s1==s2:
        return 1

    mini = min(len(s1),len(s2))
    maxi = max(len(s1),len(s2))

    for i in range(len(s1)):
        for j in range(len(s2)):
            if s1[i] == s2[j] and i not in doublon_s1 and j not in doublon_s2:
                m+=1
                doublon_s1.append(i)
                doublon_s2.append(j)

    if m==0:
        return 0
    
    for i in range(mini):
        if s1[i]==s2[i] :
            memeposition+=1

    t=(m-memeposition)/2

    return 1/3*( (m/len(s1)) + (m/len(s2)) + (m-t)/m )




def Ngram(s,n):
    substrings=[]
    for i in range(0,len(s)-n+1):
        substrings.append(s[i:i+n])
    return substrings

def NG(s1,s2,n):
    intersectionNgrams=0
    ngramS1=Ngram(s1,n)
    ngramS2=Ngram(s2,n)
    for i in ngramS1:
        if i in ngramS2:
            intersectionNgrams+=1
    return intersectionNgrams/(min(len(s1),len(s2))-n+1)




def SynonymySimilarity(s1,s2):

    Syn1 = []
    Syn2 = []

    for syn in wn.synsets(s1):
        for i in syn.lemmas():
            Syn1.append(i.name())
    
    for syn in wn.synsets(s2):
        for i in syn.lemmas():
            Syn2.append(i.name())

    for i in Syn1:
        if i in Syn2:
            return 1
    
    return 0



def CoSynonymySimilarity(s1,s2):
    Syn1 = []
    Syn2 = []
    Common = []

    for syn in wn.synsets(s1):
        for i in syn.lemmas():
            Syn1.append(i.name())
    
    for syn in wn.synsets(s2):
        for i in syn.lemmas():
            Syn2.append(i.name())

    for i in Syn1:
        if i in Syn2:
            Common.append(i)
    if len(Syn1)+len(Syn2) != 0:
        return len(Common)/(len(Syn1)+len(Syn2))
    else : 
        return 0




def ExtendedJaccard(s1,s2,seuil):

    t1 = s1.split(' ')
    t2 = s2.split(' ') # autre méthode de tokénisation peut-être ? par exemple qui prend comme séparateur les espaces, les _, les majuscules etc.

    commun = [] 
    poids = []
    uniqueS1 = []
    uniqueS2 = []

    for i in t1:
        for j in t2:
            SimiJaro = Jaro(i,j)   #Jaro pour calculer la similarité entre les tokens, parce que je savais pas trop quoi utiliser
            if SimiJaro>=seuil:
                commun.append([i,j])
                poids.append(SimiJaro)
    
    lenUniqueS1 = len(t1)-len(commun) #au lieu de faire des listes des éléments uniques, on utilise juste un entier pour compter combien il y a d'éléments uniques
    lenUniqueS2 = len(t2)-len(commun)

    SommeShared = 0
    for i in poids:
        SommeShared+=i
    return (len(commun)*SommeShared) / ((len(commun)*SommeShared) + lenUniqueS1 + lenUniqueS2)
        




def MongeElkan(s1,s2):

    t1 = s1.split(' ')
    t2 = s2.split(' ') #même chose, une autre méthode de tokénisation serait mieux
    listeMax = []

    for i in t1: 
        sigmaMax = 0
        for j in t2:
            sigma = Jaro(i,j)   #comme avant, j'utilise jaro mais n'importe quelle méthode de comparaison marcherait je pense
            if sigma > sigmaMax:
                sigmaMax = sigma
        listeMax.append(sigmaMax)
    
    sommeListe = 0
    for i in range(len(t1)):
        sommeListe += listeMax[i]

    return (1/len(t1))*sommeListe

def MongeElkanSymmetric(s1,s2):
    return (MongeElkan(s1,s2)+MongeElkan(s2,s1))/2
