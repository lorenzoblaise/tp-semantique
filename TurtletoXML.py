from rdflib import Graph
import sys

if (len(sys.argv) != 2):
    print("utilisation :TurletoXML.py fichier_turtle <fichier.ttl> ")
    exit()

g = Graph()
g.parse(sys.argv[1], format="turtle")
g.serialize(sys.argv[1].replace(".ttl", ".rdf"), format="xml")
